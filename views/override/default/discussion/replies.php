<link href="<?php echo elgg_get_site_url(); ?>mod/students/vendors/countdown/css/countdown-table.css" rel="stylesheet">

<script src="<?php echo elgg_get_site_url(); ?>mod/students/vendors/countdown/countdown.js"></script>

<?php
/**
 * List replies with optional add form
 *
 * @uses $vars['entity']        ElggEntity
 * @uses $vars['show_add_form'] Display add form or not
 */

$show_add_form = elgg_extract('show_add_form', $vars, true);

$topic = elgg_extract('entity', $vars, FALSE);


$date_time = $topic->date_time;  
$start_time = $topic->start_time;
$end_time = $topic->end_time;


$new_date_time = str_replace('-', ',' , $date_time);

$new_start_time = str_replace(':', ',', $start_time);





$exp_date = $date_time;
date_default_timezone_set('America/El_salvador'); // CDT
$timezone = date_default_timezone_get();
$todays_date = (new \DateTime('now'))->format('Y-m-d H:i:s');
$today = strtotime($todays_date);
$expiration_date = strtotime($exp_date);





if ($show_add_form) {
$mygrud =$vars['entity']->getGUID();
$typean = 'group_topic_post';
$owner_guid = elgg_get_logged_in_user_entity()->guid;
  	
	if (elgg_annotation_exists($mygrud, $typean, $owner_guid)==0)
    {

	echo '</br></br></br>';
    
        
        if ($today <= $expiration_date)
            {
		
            
        ?>
 <button class="btn btn-danger pull-right">
     <i class="fa fa-warning"></i> 
     <b style="font-size: 18px;"> 
         <?php echo elgg_echo("students:assignment:duedate"); 
     echo $date_time;?>
     </b>
 </button>
                </br></br>
</br>
                <center style="font-size: 32px; font-weight: bold;">
                    <p>
                    <?php echo elgg_echo("students:assignment:closing"); ?>
                          </p>
                </center>
                </br>

                <table id="table" border="0">
                <tr>
                <td align="center" colspan="6"><div class="numbers" id="count2" style="padding: 10px; "></div></td>
                </tr>
                <tr id="spacer1">
                <td align="center" ><div class="title" ></div></td>
                <td align="center" ><div class="numbers" id="dday"></div></td>
                <td align="center" ><div class="numbers" id="dhour"></div></td>
                <td align="center" ><div class="numbers" id="dmin"></div></td>
                <td align="center" ><div class="numbers" id="dsec"></div></td>
                <td align="center" ><div class="title" ></div></td>
                </tr>
                <tr id="spacer2">
                <td align="center" ><div class="title" ></div></td>

                <td align="center" >
                    <div class="title" id="days" style="color:black;">
                        <?php echo elgg_echo("students:assignment:days"); ?>
                    </div>
                </td>

                <td align="center" >
                    <div class="title" id="hours" style="color:black;">
                        <?php echo elgg_echo("students:assignment:hours"); ?>
                    </div>
                </td>

                <td align="center" >
                    <div class="title" id="minutes" style="color:black;">
                        <?php echo elgg_echo("students:assignment:minutes"); ?>
                    </div>
                </td>

                <td align="center" >
                    <div class="title" id="seconds" style="color:black;">
                        <?php echo elgg_echo("students:assignment:seconds"); ?>
                    </div>
                </td>

                <td align="center" ><div class="title" ></div></td>
                </tr>

            </table>
                <body onload="countdown(<?php echo elgg_echo ($new_date_time); ?>,<?php echo elgg_echo ($new_start_time);?>)"/>


	<?php
	$form_vars = array('class' => 'mtm');
	echo elgg_view_form('discussion/reply/save', $form_vars, $vars);
        
        }

    else if ($today > $expiration_date)
    {
            
        ?>

            <center>
                <p style="font-size: 32px; font-weight:bold;">
                    <?php echo elgg_echo("students:assignment:closed"); ?>
                </p>
                
                <p style="font-size: 32px; font-weight:bold;">
                    <?php echo elgg_echo("students:assignment:denied"); ?> 
                </p>
                
            </center>


            <?php

            }
        }
    else
    {
        ?>
             
            </br>
            </br>
            </br>
            </br>
            <center>
                <div class="widget widget_tally_box">
                        <div class="x_panel ui-ribbon-container fixed_height_190">
                          <div class="ui-ribbon-wrapper">
                            <div class="ui-ribbon">
                              <?php echo elgg_echo("students:assignment:ribbon"); ?>
                            </div>
                          </div>
                          <div class="x_title">
                              <h2>
                                  <?php echo elgg_echo("students:assignment:notification"); ?>
                              </h2>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">

                            <div style="text-align: center; margin-bottom: 17px">
                              <span class="chart" data-percent="86">
                                                  <span class="percent" style="font-size: 34px;">100</span>
                              </span>
                            </div>

                             

                            <div class="divider"></div>

                            <p><?php echo elgg_echo("students:assignment:successful"); ?></p>

                          </div>
                        </div>
                      </div>
                 
            </center> 
 
        <?php
    }
    }


echo '</br>';
