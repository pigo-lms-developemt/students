<?php
/**
 * Moderator role plugin language pack
 *
 */

$english = array(
	'students:role:title' => 'Estudiantes',	
        'students:assignment:successful'  => 'Tarea ha sido enviada correctamente',
        'students:assignment:closed' => 'La tarea ya esta cerrada.',
        'students:assignment:denied' => 'No se aceptan envios.',
        'students:assignment:duedate' => 'Fecha l&iacute;mite: ',
        'students:assignment:closing' => 'La tarea se cierra en:',
        'students:assignment:days' => 'D&iacute;as',
        'students:assignment:hours' => 'Horas',
        'students:assignment:minutes' => 'Minutos',
        'students:assignment:seconds' => 'Segundos',
        'students:assignment:ribbon' => '&Eacute;xito',
        'students:assignment:notification' => 'Notificaci&oacute;n',
);

add_translation("es", $english);
